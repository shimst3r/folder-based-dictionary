#!/usr/bin/env bats

@test "calling fbd without arguments shows usage text" {
  run bash fbd.sh

  [ "$status" -eq 1 ]
  [ "$output" = "usage: fbd [init <root-path> | create <key> <value> | read <key> | update <key> <value> | delete <key>]" ]
}

@test "calling fbd init for already existing root-path returns error" {
  root=root_dir
  mkdir "$root"

  run bash fbd.sh init $root

  [ "$status" -eq 1 ]
  [ "$output" = "$(realpath $root) already exists." ]

  rm -rf "$root"
}
@test "calling fbd init creates a directory at root-path" {
  root=root_dir
  ! [ -d "$root" ]

  run bash fbd.sh init $root

  [ "$status" -eq 0 ]
  [ "$output" = "Initialized key-value store at $(realpath $root)" ]
  [ -d "$root" ]

  rm -rf "$root"
}
