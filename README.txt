--------------------------------------------------------------------------------
fbd_init()

Private: Initializes the key-value store by creating its root directory.

Takes a single argument and creates a directory in its place.

$1 - root directory for newly created key-value store

Examples

   fbd-init /tmp/fbd_test

Returns 0 if the key-value store got created as expected, 1 otherwise.

--------------------------------------------------------------------------------
main()

Public: Entrypoint for the fbd API

Exposes a CRUD interface to create, read, update, or delete key-value pairs.

$1 - One of init, create, read, update, delete
$2 - depends on $1
$3 - depends on $1

Examples

   fbd init /tmp/fbd_test
   fbd create key value
   fbd read key
   fbd update key new_value
   fbd delete key

Returns 0 if the subcommand resolved as expected, 1 otherwise.

