#!/usr/bin/env bash

# Private: Initializes the key-value store by creating its root directory.
#
# Takes a single argument and creates a directory in its place.
#
# $1 - root directory for newly created key-value store
#
# Examples
#
#    fbd-init /tmp/fbd_test
#
# Returns 0 if the key-value store got created as expected, 1 otherwise.
fbd_init() {
  if [[ -d $1 ]]; then
	  echo "$(realpath $1) already exists."
	  exit 1
  fi

  mkdir "$1"
  echo "Initialized key-value store at $(realpath $1)"
}


# Public: Entrypoint for the fbd API
#
# Exposes a CRUD interface to create, read, update, or delete key-value pairs.
#
# $1 - One of init, create, read, update, delete
# $2 - depends on $1
# $3 - depends on $1
#
# Examples
#
#    fbd init /tmp/fbd_test
#    fbd create key value
#    fbd read key
#    fbd update key new_value
#    fbd delete key
#
# Returns 0 if the subcommand resolved as expected, 1 otherwise.
main() {
	if [[ $# -eq 0 ]]; then
		echo "usage: fbd [init <root-path> | create <key> <value> | read <key> | update <key> <value> | delete <key>]"
		exit 1
	fi

	case $1 in
	"init")
		fbd_init "$2"
		;;
	esac
}

main "$@"
